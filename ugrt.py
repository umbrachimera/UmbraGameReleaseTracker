#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# Import lots of things
from __future__ import print_function
import os
import datetime
import time
import httplib2
import csv
import tweepy
from shutil import copyfile
import pandas as pd
from apiclient import discovery
import re
import requests
from requests import get
from bs4 import BeautifulSoup
import private

try:
    os.remove('gameDB.csv')
except OSError:
    pass

with open('gameDB.csv', 'a') as gameDB:
    myFields = ['EventID', 'Console', 'GameName', 'rDay', 'Month', 'rYear', 'CombinedDate', 'GCalLink', 'BoxArt']
    writer = csv.DictWriter(gameDB, fieldnames=myFields)
    writer.writeheader()


# Setup Google OAuth2
def goo_cred():
    # Import Everything needed for Google OAuth2
    import os
    from oauth2client.file import Storage

    # Get OAuth2 Credentials
    home_dir = os.getcwd()
    credential_path = os.path.join(home_dir, 'gOAuth.json')
    store = Storage(credential_path)
    credentials = store.get()
    return credentials
# Set Credentials for Google OAuth2
goocredentials = goo_cred()
http = goocredentials.authorize(httplib2.Http())
service = discovery.build('calendar', 'v3', http=http)
# Set Credentials for Twitter API
tCONSUMER_KEY = private.twitter['CONSUMER_KEY']
tCONSUMER_SECRET = private.twitter['CONSUMER_SECRET']
tACCESS_KEY = private.twitter['ACCESS_KEY']
tACCESS_SECRET = private.twitter['ACCESS_SECRET']
tauth = tweepy.OAuthHandler(tCONSUMER_KEY, tCONSUMER_SECRET)
tauth.set_access_token(tACCESS_KEY, tACCESS_SECRET)
tapi = tweepy.API(tauth)


def dateChange(rMonth):
    if (rMonth == 'Jan' or rMonth == 'January'):
        nMonth = '01'
    elif (rMonth == 'Feb' or rMonth == 'February'):
        nMonth = '02'
    elif (rMonth == 'Mar' or rMonth == 'March'):
        nMonth = '03'
    elif (rMonth == 'Apr' or rMonth == 'April'):
        nMonth = '04'
    elif (rMonth == 'May'):
        nMonth = '05'
    elif (rMonth == 'Jun' or rMonth == 'June'):
        nMonth = '06'
    elif (rMonth == 'Jul' or rMonth == 'July'):
        nMonth = '07'
    elif (rMonth == 'Aug' or rMonth == 'August'):
        nMonth = '08'
    elif (rMonth == 'Sep' or rMonth == 'September'):
        nMonth = '09'
    elif (rMonth == 'Oct' or rMonth == 'October'):
        nMonth = '10'
    elif (rMonth == 'Nov' or rMonth == 'November'):
        nMonth = '11'
    elif (rMonth == 'Dec' or rMonth == 'December'):
        nMonth = '12'
    else:
        nMonth = rMonth
    return nMonth


# Function to make a goo.gl link
def google_url_shorten(url):
    import json
    GOOGLE_URL_SHORTEN_API = private.google['googlAPi']
    req_url = 'https://www.googleapis.com/urlshortener/v1/url?key=' + GOOGLE_URL_SHORTEN_API
    payload = {'longUrl': url}
    headers = {'content-type': 'application/json'}
    r = requests.post(req_url, data=json.dumps(payload), headers=headers)
    resp = json.loads(r.text)
    return resp['id']


def WriteDB(eventID, console, gameName, rDay, rMonth, rYear, gameRelease, shortURL, boxArt):
    is_in_file = False
    with open('gameDB.csv', 'rt') as gameDB:
        my_content = csv.reader(gameDB, delimiter=',')
        for row in my_content:
            if eventID in row:
                is_in_file = True
                break
    if is_in_file is False:
        with open('gameDB.csv', 'a') as gameDB:
            myFields = ['EventID', 'Console', 'GameName', 'rDay', 'Month', 'rYear', 'CombinedDate', 'GCalLink', 'BoxArt']
            writer = csv.DictWriter(gameDB, fieldnames=myFields)
            writer.writerow({'EventID': eventID, 'Console': console, 'GameName': gameName, 'rDay': rDay, 'Month': rMonth, 'rYear': rYear, 'CombinedDate': gameRelease, 'GCalLink': shortURL, 'BoxArt': boxArt})
    else:
        pass


def twitGames(boxArt, tweet):
    filename = 'temp.jpg'
    request = requests.get(boxArt, stream=True)
    if request.status_code == 200:
        with open(filename, 'wb') as image:
            for chunk in request:
                image.write(chunk)

        tapi.update_with_media(filename, status=tweet)
        os.remove(filename)
        time.sleep(300)
    else:
        tapi.update_status(tweet)
        time.sleep(300)


def twitUpdate(tDay):
    import datetime
    x = 0
    margin1 = datetime.timedelta(days=1)
    margin3 = datetime.timedelta(days=3)
    margin7 = datetime.timedelta(days=7)
    df = pd.read_csv('gameDB.csv')
    rows = len(df.index)
    while (x < rows):
        dConsole = str((df.iloc[x, 1]))
        dName = str((df.iloc[x, 2]))
        dDate = str((df.iloc[x, 6]))
        dCal = str((df.iloc[x, 7]))
        dArt = str((df.iloc[x, 8]))
        from datetime import datetime
        today = datetime.strptime(tDay, '%Y-%m-%d').date()
        try:
            compDate = datetime.strptime(dDate, '%Y-%m-%d').date()
        except:
            compDate = datetime.strptime('1977-01-01', '%Y-%m-%d')

        if (today+margin7 == compDate):
            tweet = (dName + ' for the ' + dConsole + ' comes out next week. Click here to add to calendar ' + dCal)
            print (tweet)
            twitGames(dArt, tweet)
            time.sleep(900)
        elif (today+margin3 == compDate):
            tweet = (dName + ' for the ' + dConsole + ' releases in 3 days. Click here to add to calendar ' + dCal)
            print (tweet)
            twitGames(dArt, tweet)
            time.sleep(900)
        elif (today+margin1 == compDate):
            tweet = (dName + ' for the ' + dConsole + ' releases tomorrow. Click here to add to calendar ' + dCal)
            print (tweet)
            twitGames(dArt, tweet)
            time.sleep(900)
        elif (today == compDate):
            tweet = (dName + ' for the ' + dConsole + ' is now released.')
            print (tweet)
            twitGames(dArt, tweet)
            time.sleep(900)
        x += 1


# Function for Nintendo Switch
def NintendoSwitch():
    system = 'Nintendo Switch'
    data1 = private.gameID['SwitchURL1']
    color = '11'
    calID = private.gameID['SwitchCal']
    URL1(system, data1, color, calID)
    # URL2(system, data2, color, calID))


# Function for Playstation 4
def PS4():
    system = 'Playstation 4'
    data1 = private.gameID['PS4URL1']
    color = '9'
    calID = private.gameID['PS4Cal']
    URL1(system, data1, color, calID)
    # URL2(system, data2, color, calID)


# Function for Xbox One
def XboxOne():
    system = 'Xbox One'
    data1 = private.gameID['X1URL1']
    color = '10'
    calID = private.gameID['X1Cal']
    URL1(system, data1, color, calID)
    # URL2(system, data2, color, calID)


# Function for 3DS
def n3ds():
    system = '3DS'
    data1 = private.gameID['3DSURL1']
    color = '8'
    calID = private.gameID['3DSCal']
    URL1(system, data1, color, calID)
    # URL2(system, data2, color, calID))


# Function for PC
def PC():
    system = 'PC'
    data1 = private.gameID['PCURL1']
    color = '7'
    calID = private.gameID['PCCal']
    URL1(system, data1, color, calID)
    # URL2(system, data2, color, calID))


# Function to scrape the release data
def URL1(console, url, calColor, consoleID):
    gameRow = 0
    track = 0
    nPage = 0
    response = get(url, headers={'User-Agent': 'Mozilla/5.0'})
    html_soup = BeautifulSoup(response.text, 'html.parser')
    runTimes = 200
    try:
        while (track < runTimes):

            if (gameRow == 25):
                if not (console is '3DS'):
                    nPage = ((int(nPage)) + 25)
                    gameRow = 0
                    response = get((url + str(nPage)), headers={'User-Agent': 'Mozilla/5.0'})
                    html_soup = BeautifulSoup(response.text, 'html.parser')
            main(html_soup, gameRow, console, url, calColor, consoleID)
            gameRow += 1
            track += 1
    except IndexError:
        pass


def main(html_soup, gameRow, console, url, calColor, consoleID):
    game_containers = html_soup.find_all('div', class_='clear itemList-item')
    upcomingGame = game_containers[gameRow]
    name_containers = upcomingGame.find('div', class_="item-title").h3.a.text.strip()
    date_containers = upcomingGame.find('div', class_="releaseDate grid_3 omega").text.strip()
    box_art = upcomingGame.find('img', class_="item-boxArt")
    try:
        boxArt = (box_art["src"])
    except:
        boxArt = ''
    gameName = name_containers
    conGameName = (console + ' - ' + gameName)
    dateName = date_containers
    try:
        monthDay, rYear = dateName.split(", ")
        rMonth, rDay = monthDay.split(" ")
    except:
        try:
            rMonth, rYear = dateName.split(" ")
            rDay = ""
        except:
            try:
                rYear = str(dateName.split(" "))
                rMonth = ""
                rDay = ""
            except Exception as e:
                print (e)
                input()
    nMonth = dateChange(rMonth)
    rYear = rYear.strip()
    gameRelease = rYear + '-' + nMonth + '-' + rDay
    eventID = ('ucid' + conGameName + 'v5')
    eventID = re.sub('[^a-zA-Z0-9]+', '', eventID)
    eventID = re.sub('[w-zW-Z]', '', eventID).lower()
    # Merged Calendar, loads all games into one unified calendar, color codes them
    # Then tweets out a link to the event
    if not (console is 'PC'):
        merID = 'mer' + eventID
        try:  # Cannot create the event if it already exist, which also prevent duplicate tweets
            event = {
                  'id': merID,
                  'summary': conGameName,
                  'start': {
                    'date': gameRelease,
                  },
                  'end': {
                    'date': gameRelease,
                  },
                  'reminders': {
                    'useDefault': False,
                  },
                  'transparency': 'transparent',
                  'visibility': 'public',
                  'colorId': calColor,
                }
            event = service.events().insert(calendarId='primary', body=event).execute()
            longURL = (event.get('htmlLink'))
            shortURL = str((google_url_shorten(longURL)))
            print('Adding Event for ' + gameName)
            tweet = (gameName + ' for the ' + console + ' comes out on ' + gameRelease + '! ' + shortURL)
            twitGames(boxArt, tweet)
        except:
            pass
        try:  # this allows it to still generate a CalURL for sharing if the event already existed
            event = service.events().get(calendarId='primary', eventId=merID).execute()
            longURL = (event.get('htmlLink'))
            shortURL = str((google_url_shorten(longURL)))
        except:
            shortURL = ''
            pass
# Second calendar that sorts based on platform
# Does not tweet out link as it would be redundant
    try:  # Cannot create the event if it already exist, which also prevent duplicate tweets
        event = {
              'id': eventID,
              'summary': conGameName,
              'start': {
                'date': gameRelease,
              },
              'end': {
                'date': gameRelease,
              },
              'reminders': {
                'useDefault': False,
              },
              'transparency': 'transparent',
              'visibility': 'public',
              'colorId': calColor,
            }
        event = service.events().insert(calendarId=consoleID, body=event).execute()
        longURL = (event.get('htmlLink'))
        shortURL = str((google_url_shorten(longURL)))
        print('Adding Event for ' + gameName)
    except:
        pass
    try:  # this allows it to still generate a CalURL for sharing if the event already existed
        event = service.events().get(calendarId=consoleID, eventId=eventID).execute()
        longURL = (event.get('htmlLink'))
        shortURL = str((google_url_shorten(longURL)))
    except:
        shortURL = ''
        pass
    WriteDB(eventID, console, gameName, rDay, rMonth, rYear, gameRelease, shortURL, boxArt)


# Call the platforms you want info for
print('Started DB search for Switch')
NintendoSwitch()
print ('Done with Switch')
print('Started DB search for PS4')
PS4()
print ('Done with PS4')
print('Started DB search for X1')
XboxOne()
print ('Done with X1')
print('Started DB search for 3DS')
n3ds()
print ('Done with 3DS')
print('Started DB search for PC, takes a while')
PC()
print ('Done with PC')
copyfile('gameDB.csv', 'saved_gameDB.csv')
print('Started DB search for twitter updates')
tDay = str(datetime.date.today())
twitUpdate(tDay)
print('Done with updates')

