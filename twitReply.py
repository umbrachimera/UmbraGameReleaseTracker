#!/usr/bin/python3.6
# -*- coding: utf-8 -*-


def LiveTweeting():
    from tweepy.streaming import StreamListener
    from tweepy import OAuthHandler
    from tweepy import Stream
    from tweepy import API
    import private
    import json
    import time

    consumer_key = private.twitter['CONSUMER_KEY']
    consumer_secret = private.twitter['CONSUMER_SECRET']
    access_token = private.twitter['ACCESS_KEY']
    access_token_secret = private.twitter['ACCESS_SECRET']
    account_user_id = private.twitter['ACCOUNT_USER_ID']

    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    twitterApi = API(auth)

    class ReplyToTweet(StreamListener):

        def on_data(self, data):
            print (data)
            tweet = json.loads(data.strip())

            retweeted = tweet.get('retweeted')
            from_self = tweet.get('user', {}).get('id_str', '') == account_user_id

            if retweeted is not None and not retweeted and not from_self:

                tweetId = tweet.get('id_str')
                screenName = tweet.get('user', {}).get('screen_name')
                tweetText = tweet.get('text')
                tweetText = tweetText.replace('@UmbraGameTrack ', '')
                print (tweetText)
                chatResponse = checkDB(tweetText)

                replyText = '@' + screenName + ' ' + chatResponse

                # check if repsonse is over 140 char
                if len(replyText) > 140:
                    replyText = replyText[0:139] + '…'

                print('Tweet ID: ' + tweetId)
                print('From: ' + screenName)
                print('Tweet Text: ' + tweetText)
                print('Reply Text: ' + replyText)

                # If rate limited, the status posts should be queued up and sent on an interval
                twitterApi.update_status(status=replyText, in_reply_to_status_id=tweetId)
                time.sleep(30)

        def on_error(self, status):
            print (status)

    if __name__ == '__main__':
        streamListener = ReplyToTweet()
        twitterStream = Stream(auth, streamListener)
        twitterStream.userstream(_with='user')


def checkDB(tweetText):
    import pandas as pd
    df = pd.read_csv('saved_gameDB.csv', encoding="UTF-8")
    rows = len(df.index)
    x = 0
    twitReply = "I'm sorry I couldn't find that for you."
    while (x < rows):
        dConsole = str((df.iloc[x, 1]))
        dName = str((df.iloc[x, 2]))
        dDate = str((df.iloc[x, 6]))
        dCal = str((df.iloc[x, 7]))
        # dArt = str((df.iloc[x, 8]))

        if tweetText.lower() in (dName.lower()):
            twitReply = (dName + ' comes out on ' + dDate + ' for the ' + dConsole + '. Here is a calendar link: ' + dCal)
            break
        x += 1
    return twitReply

LiveTweeting()
