# Replace with your API Keys obtained from https://apps.twitter.com
twitter = {
    'CONSUMER_KEY': '#####################################',
    'CONSUMER_SECRET': '#####################################',
    'ACCESS_KEY': '#####################################',
    'ACCESS_SECRET': '#####################################',
    'RULE' : '@TWITTER_HANDLE',
    'ACCOUNT_SCREEN_NAME' : "YOUR SCREEN NAME",
    'ACCOUNT_USER_ID': "YOUR OWNDER ID, ##########",
    }

# This is used for the Google URL Shortner service to make goo.gl links
# Replace with your Google API Key that you generate here https://console.developers.google.com
google = {
    'googlAPi': '#####################################',
}

''' Format is
    'AbreviatonURL': 'URL Where you pull the information from',
    'AbrevaitionCal': ' Link to the unqiue Google Calendar ID assocaited'
'''
gameID = {
    'SwitchURL': '#####################################',
    'SwitchCal': '#####################################',
    'PS4URL': '#####################################',
    'PS4Cal': '#####################################',
    'X1URL': '#####################################',
    'X1Cal': '#####################################',
    '3DSURL': '#####################################',
    '3DSCal': '#####################################',
    'PCURL': '#####################################',
    'PCCal': '#####################################'
    }
